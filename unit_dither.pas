unit unit_dither;

interface

//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    You can find PETSCIIFier here:
//    https://bitbucket.org/paul_nicholls/petasciifier/src/master/
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

uses
  unit_ABGR,Vcl.ExtCtrls,Vcl.ComCtrls;

type
    TPalette = array of TABGR;

function  NewPalette(const Colors: array of TABGR): TPalette;
procedure FindNearestColor(color: TABGR; palette: TPalette; var aColor: TABGR; var aIndex: Integer);
procedure floydSteinberg(DstImage: TImage; palette: TPalette; StatusBar: TStatusBar);

implementation

uses
  System.SysUtils;

function  NewPalette(const Colors: array of TABGR): TPalette;
var
  i : Integer;
begin
  SetLength(Result,Length(Colors));

  for i := 0 to Length(Colors) - 1 do begin
    Result[i] := Colors[i];
  end;
end;

//---------------------------------------
// code adapted from here:
// https://forums.getpaint.net/topic/29428-floyd-steinberg-dithering-including-source/
// BoltBait's Floyd-Steinberg Dithering Effect v1.0
//---------------------------------------

function AddAndTruncate(a: Byte; b: Integer): Byte;
var
  c : Integer;
begin
  c := a + b;
  if (c < 0)   then
    Result := 0
  else
  if (c > 255) then
    Result := 255
  else
    Result := Byte(c);
end;

procedure FindNearestColor(color: TABGR; palette: TPalette; var aColor: TABGR; var aIndex: Integer);
var
  minDistanceSquared : Integer;
  distanceSquared    : Integer;
  bestIndex          : Integer;
  i                  : Integer;
  Rdiff              : Integer;
  Gdiff              : Integer;
  Bdiff              : Integer;
begin
  minDistanceSquared := 255 * 255 + 255 * 255 + 255 * 255 + 1;
  bestIndex          := 0;

  for i := 0 to Length(palette) - 1 do begin
    Rdiff := color.R - palette[i].R;
    Gdiff := color.G - palette[i].G;
    Bdiff := color.B - palette[i].B;

    distanceSquared := Rdiff * Rdiff + Gdiff * Gdiff + Bdiff * Bdiff;

    if (distanceSquared < minDistanceSquared) then begin
      minDistanceSquared := distanceSquared;
      bestIndex := i;
      if (minDistanceSquared < 1) then break;
    end;
  end;

  aIndex := bestIndex;
  aColor := palette[bestIndex];
end;

function ClipValue(v,min,max: Integer): Integer;
begin
  Result := v;

  if (Result < min) then
    Result := min
  else
  if (Result > max) then
    Result := max;
end;

procedure MultiplyPixelByError(AImage : TImage; x,y: Integer; error: TABGR; mul,shift: Integer);
var
  Pixels : PABGRArray;
  r,g,b  : Integer;
begin
  if (x < 0) then Exit;
  if (y < 0) then Exit;
  if (x >= AImage.Width)  then Exit;
  if (y >= AImage.Height) then Exit;

  Pixels := AImage.Picture.Bitmap.ScanLine[y];

  r := Pixels^[x].r + (error.r * mul shr shift);
  g := Pixels^[x].g + (error.g * mul shr shift);
  b := Pixels^[x].b + (error.b * mul shr shift);

  r := ClipValue(r,0,255);
  g := ClipValue(g,0,255);
  b := ClipValue(b,0,255);

  Pixels^[x].r := r;
  Pixels^[x].g := g;
  Pixels^[x].b := b;
end;

procedure floydSteinberg(DstImage: TImage; palette: TPalette; StatusBar: TStatusBar);
// code created from here:
//
//https://www.visgraf.impa.br/Courses/ip00/proj/Dithering1/floyd_steinberg_dithering.html
var
  x,y      : Integer;
  error    : TABGR;
  oldpixel : TABGR;
  newpixel : TABGR;
  Pixels   : PABGRArray;
  index    : Integer;
  i,maxI   : Integer;
begin
  i    := 0;
  maxI := DstImage.Width * DstImage.Height;

  x  := 0;
  // do dithering
  for y := 0 to DstImage.Height - 1 do begin
    Pixels := DstImage.Picture.Bitmap.ScanLine[y];

    for x := 0 to DstImage.Width - 1 do begin
     oldpixel   := Pixels^[x];
      FindNearestColor(oldpixel,palette,newpixel,index);
      Pixels^[x] := newPixel;

      // error between old and new pixel
      error.r    := oldpixel.r - newpixel.r;
      error.g    := oldpixel.g - newpixel.g;
      error.b    := oldpixel.b - newpixel.b;

        // dx = 1
        //  - * 7    where *=pixel being processed, -=previously processed pixel
        //  3 5 1    and pixel difference is distributed to neighbor pixels
        //           Note: 7+3+5+1=16 so we divide by 16 (shr 4) before addi7ng.

        //  dx = -1
        //  7 *      where *=pixel being processed, -=previously processed pixel
        //  1 5 3    and pixel difference is distributed to neighbor pixels
        //           Note: 7+3+5+1=16 so we divide by 16 (shr 4) before addi7ng.


        MultiplyPixelByError(DstImage,x + 1,y    ,error,7,4);
        MultiplyPixelByError(DstImage,x - 1,y + 1,error,3,4);
        MultiplyPixelByError(DstImage,x    ,y + 1,error,5,4);
        MultiplyPixelByError(DstImage,x + 1,y + 1,error,1,4);
      Inc(i);
    end;
    StatusBar.SimpleText := 'Dithering: '+IntToStr(100*i div maxI) + '%';
  end;

  DstImage.Invalidate;
end;

end.
