unit unit_png_loader;

interface

//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    You can find PETSCIIFier here:
//    https://bitbucket.org/paul_nicholls/petasciifier/src/master/
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

uses
  unit_ImageBuffer32;

function LoadPNGFromFile(FileName: String; Buffer: TImageBuffer32): Boolean;

implementation

uses
  Classes,
  SysUtils,
  BeRoPNG;

function LoadPNGFromFile(FileName: String; Buffer: TImageBuffer32): Boolean;
var
  PNGFile  : TMemoryStream;
  PNGData  : Pointer;
  PNGWidth : Integer;
  PNGHeight: Integer;
  PNGAddr  : PRGBA;
  BufferRow: PRGBAArray;
  x,y      : Integer;
begin
  Result := False;

  if not FileExists(FileName) then Exit;

  PNGFile := TMemoryStream.Create;
  try
    PNGFile.LoadFromFile(FileName);
    Result := LoadPNG(PNGFile.Memory,PNGFile.Size,PNGData,PNGWidth,PNGHeight,False);
    if not Result then Exit;

    Buffer.SetSize(PNGWidth,PNGHeight);

    PNGAddr := PNGData;
    // copy bitmap pixels to buffer pixels
    for y := 0 to PNGHeight - 1 do
    begin
      BufferRow := Buffer.ScanLine[y];
      for x := 0 to PNGWidth - 1 do
      begin
        BufferRow^[x].r := PNGAddr^.r;
        BufferRow^[x].g := PNGAddr^.g;
        BufferRow^[x].b := PNGAddr^.b;
        BufferRow^[x].a := PNGAddr^.a;
        Inc(PNGAddr);
      end;
    end;
  finally
    PNGFile.Free;
  end;
end;

end.

