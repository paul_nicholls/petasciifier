unit form_welcome;

interface
//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    You can find PETSCIIFier here:
//    https://bitbucket.org/paul_nicholls/petasciifier/src/master/
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Imaging.pngimage;

type
  TDialogWelcome = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    OKBtn: TButton;
    Panel2: TPanel;
    ProgramIcon: TImage;
    Label1: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    ShowWelcome_CheckBox: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogWelcome: TDialogWelcome;

implementation

uses
  inifiles;

{$R *.dfm}

procedure TDialogWelcome.FormClose(Sender: TObject; var Action: TCloseAction);
// save state of show welcome on startup checkbox for next time
var
  iniFile : TIniFile;
begin
  //open ini file and store status of ShowWelcome checkbox
  iniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  try
    iniFile.WriteBool('OnLoad','ShowWelcome',ShowWelcome_CheckBox.Checked);
  finally
    iniFile.Free;
  end;
end;

procedure TDialogWelcome.FormCreate(Sender: TObject);
var
  iniFile : TIniFile;
begin
  // open ini file and check for ShowWelcome status
  iniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  try
    if (not iniFile.SectionExists('OnLoad')) then
    // no inifile so load automatically
      ShowWelcome_CheckBox.Checked := True
    else begin
      ShowWelcome_CheckBox.Checked := iniFile.ReadBool('OnLoad','ShowWelcome',True);
    end;
  finally
    iniFile.Free;
  end;
end;

end.
