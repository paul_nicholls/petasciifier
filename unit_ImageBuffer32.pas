unit unit_ImageBuffer32;
{$ifdef fpc}
{$mode Delphi}
{$endif}
{$H+}

//-----------------------------------
// unit_ImageBuffer32
// Written by Paul Nicholls
//-----------------------------------

interface

//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

type
  PRGBA = ^TRGBA;
  TRGBA = packed record
    case Integer of
      0 : (r,g,b,a: Byte);
      1 : (Value: LongWord);
  end;

  PRGBAArray = ^TRGBAArray;
  TRGBAArray = array[0..(MaxInt div SizeOf(TRGBA)) - 1] of TRGBA;

  TImageBuffer32 = class
  private
    FWidth  : Word;
    FHeight : Word;
    FPixels : array of TRGBA;

    function  GetScanLine(y: Word): Pointer;
    function  GetBit(Index: Integer): TRGBA;
    procedure SetBit(Index: Integer; Color: TRGBA);
  public
    constructor Create;
    destructor  Destroy; override;

    procedure SetSize(aWidth,aHeight: Word);
    procedure SetBitRGBA(Index: Integer; r,g,b,a: Byte);
    function  GetPixels: Pointer;

    function  LoadFromFile(FileName: String): Boolean;

    property Width : Word read FWidth;
    property Height: Word read FHeight;
    property ScanLine[y: Word]: Pointer  read GetScanLine;
    property Bits[Index: Integer]: TRGBA read GetBit write SetBit;
  end;

implementation

uses
  SysUtils,
  unit_bmp_loader,
  unit_tga_loader,
  unit_png_loader;

function  NewRGBA(r,g,b,a: Byte): TRGBA;
begin
  Result.r := r;
  Result.g := g;
  Result.b := b;
  Result.a := a;
end;

constructor TImageBuffer32.Create;
begin
  inherited Create;

  SetSize(0,0);
end;

destructor  TImageBuffer32.Destroy;
begin
  SetSize(0,0);

  inherited Destroy;
end;

procedure TImageBuffer32.SetSize(aWidth,aHeight: Word);
var
  i: Integer;
begin
  FWidth  := aWidth;
  FHeight := aHeight;

  SetLength(FPixels,FWidth * FHeight);

  for i := 0 to FWidth * FHeight - 1 do
    FPixels[i].Value := 0;
end;

function  TImageBuffer32.GetScanLine(y: Word): Pointer;
begin
  Result := @FPixels[y * FWidth];
end;

function  TImageBuffer32.GetBit(Index: Integer): TRGBA;
begin
  Result := FPixels[Index];
end;

procedure TImageBuffer32.SetBit(Index: Integer; color: TRGBA);
begin
  FPixels[Index] := color;
end;

procedure TImageBuffer32.SetBitRGBA(Index: Integer; r,g,b,a: Byte);
begin
  FPixels[Index].r := r;
  FPixels[Index].g := g;
  FPixels[Index].b := b;
  FPixels[Index].a := a;
end;

function  TImageBuffer32.GetPixels: Pointer;
begin
  Result := nil;

  if Length(FPixels) = 0 then Exit;

  Result := @FPixels[0];
end;

function  TImageBuffer32.LoadFromFile(FileName: String): Boolean;
var
  Ext: String;
begin
  Result := False;

  if not FileExists(FileName) then Exit;

  Ext := LowerCase(ExtractFileExt(FileName));

  if      Ext = '.bmp' then Result := LoadBMPFromFile(FileName,Self)
  else if Ext = '.tga' then Result := LoadTGAFromFile(FileName,Self)
  else if Ext = '.png' then Result := LoadPNGFromFile(FileName,Self);
end;

end.
