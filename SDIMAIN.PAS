unit SDIMAIN;

interface
//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    You can find PETSCIIFier here:
//    https://bitbucket.org/paul_nicholls/petasciifier/src/master/
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

uses Winapi.Windows, System.Classes, Vcl.Graphics, Vcl.Forms, Vcl.Controls,
  Vcl.Menus, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.ImgList, Vcl.StdActns, Vcl.ActnList, Vcl.ToolWin, System.ImageList,
  System.Actions,
  System.Contnrs,unit_ImageBuffer32, Vcl.Samples.Spin,unit_ABGR,unit_dither,form_welcome;

type
  TColorObject = class
    a,b,g,r:   Byte;
    frequency: Integer;
  end;

  TPETSCIIBlock = record
    characterCode : Integer;
    colorIndex    : Integer;
  end;

  PCharSet = ^TCharSet;
  TCharSet = array[0..256 * 8 - 1] of Byte;

  TSetPixelBlock = array[0..7,0..7] of Boolean;

  TSDIAppForm = class(TForm)
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ActionList1: TActionList;
    FileOpen1: TAction;
    FileSaveAs1: TAction;
    FileExit1: TAction;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    HelpAbout1: TAction;
    StatusBar: TStatusBar;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    FileOpenItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    N1: TMenuItem;
    FileExitItem: TMenuItem;
    Edit1: TMenuItem;
    CopyItem: TMenuItem;
    PasteItem: TMenuItem;
    Help1: TMenuItem;
    HelpAboutItem: TMenuItem;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    ScrollBox1: TScrollBox;
    ScrollBox2: TScrollBox;
    ScrollBox3: TScrollBox;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Original_Image: TImage;
    PETSCIIFIED_Image: TImage;
    CharSet_Image: TImage;
    Panel2: TPanel;
    LoadImage_Button: TButton;
    Panel3: TPanel;
    PETSCIIFy_Button: TButton;
    Panel4: TPanel;
    LoadCharSet_Button: TButton;
    PopupMenu1: TPopupMenu;
    CopyImage_PopupMenuItem: TMenuItem;
    PopupMenu2: TPopupMenu;
    PasteImage_PopupMenu: TMenuItem;
    Colors_RadioGroup: TRadioGroup;
    ExportData1: TMenuItem;
    KickAssemblyASM_PopupMenu: TMenuItem;
    Welcome1: TMenuItem;
    KickAssemblyBinaryfiles1: TMenuItem;
    SaveAs1: TMenuItem;
    GroupBox4: TGroupBox;
    DitheringPalette_ComboBox: TComboBox;
    UseDithering_CheckBox: TCheckBox;
    ReplacementPalette_ComboBox: TComboBox;
    Threshold_SpinEdit: TSpinEdit;
    Threshold_CheckBox: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Width_Edit: TEdit;
    Height_Edit: TEdit;
    procedure FileExit1Execute(Sender: TObject);
    procedure HelpAbout1Execute(Sender: TObject);
    procedure LoadImage_ButtonClick(Sender: TObject);
    procedure LoadCharSet_ButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PETSCIIFy_ButtonClick(Sender: TObject);
    procedure CopyImage_PopupMenuItemClick(Sender: TObject);
    procedure PasteImage_PopupMenuClick(Sender: TObject);
    procedure KickAssemblyASM_PopupMenuClick(Sender: TObject);
    procedure PETSCIIFIED_ImageMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FileSave1Execute(Sender: TObject);
    procedure Welcome1Click(Sender: TObject);
    procedure KickAssemblyBinaryfiles1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure UseDithering_CheckBoxClick(Sender: TObject);
    procedure ReplacementPalette_ComboBoxClick(Sender: TObject);
    procedure DitheringPalette_ComboBoxClick(Sender: TObject);
    procedure Threshold_CheckBoxClick(Sender: TObject);
  private
    { Private declarations }
    DialogWelcome : TDialogWelcome;
    // raw character set binary data
    RawCharSet: TCharSet;
    // each 8x8 block of on/off pixels for each character in charset
    CharacterSetPixelBlocks: array[0..255] of TSetPixelBlock;
    // default pixel color for on pixels in charset
    CharPixelOnColor: TABGR;
    CharPixelOffColor: TABGR;

    //color palettes
    C64Palette         : TPalette;
    TwoTonePalette     : TPalette;
    Gray4ShadesPalette : TPalette;
    Gray8ShadesPalette : TPalette;
    CMYKPalette        : TPalette;
    WindowsPalette     : TPalette;
    Custom16Palette    : TPalette;

    DitheringPalette   : TPalette;
    ReplacementPalette : TPalette;

    PETSCIIFiedData        : array of array of TPETSCIIBlock;
    PETSCIIFiedDataW       : Integer;
    PETSCIIFiedDataH       : Integer;
    UseOriginalImageColors : Boolean;
    OriginalImageSet: Boolean;

    procedure SetupPalettes;
    procedure ShowWelcomeOnStart;
    procedure LoadBMPFromFile(FileName: String; Image: TImage);
    procedure LoadPNGFromFile(FileName: String; Image: TImage);
    procedure SaveImageAsBMP(FileName: String; Image: TImage);
    procedure SaveImageAsPNG(FileName: String; Image: TImage);
    procedure SaveImageToFile(FileName: String; Image: TImage);
    procedure InitPETSCIIfiedImage(SrcImage,DstImage: TImage);
    function  GetSetPixelsBlockWithThresholdAt(SrcImage: TImage; bx,by: Integer; Threshold: Single): TSetPixelBlock;
    function  GetSetPixelsBlockAt(SrcImage: TImage; bx,by: Integer; ColorToMatch: TABGR): TSetPixelBlock;
    function  GetPixelsBlockMatchPercentage(ImagePixelsBlock,CharPixelsBlock: TSetPixelBlock): Integer;
    function  GetAverageColorOfBlockAt(SrcImage: TImage; bx,by: Integer): TABGR;
    procedure ReplaceImageColors(DstImage: TImage; palette: TPalette; StatusBar: TStatusBar);
    function  PETSCIIFyBlockAt(SrcImage,DstImage: TImage; bx,by: Integer; ColorToMatch,AverageColor: TABGR): TPETSCIIBlock;
    procedure AddColorToList(AColor: TABGR; AList: TObjectList);
    function  GetMostUsedColourInBlockAt(SrcImage: TImage; bx,by: Integer): TABGR;
    function  GetCharSetPixel(CharSet: PCharSet; c,x,y: Integer): Boolean;
    procedure ConvertCharSetToImage(CharSet: PCharSet; Image: TImage);
    procedure LoadCharacterSet(FileName: String);
    procedure LoadOriginalImage(FileName: String);
    procedure CopyBufferToImage(Buffer: TImageBuffer32; Image: TImage);
  public
    { Public declarations }
  end;

var
  SDIAppForm: TSDIAppForm;

implementation

uses
  About,System.SysUtils,System.Math,Vcl.Clipbrd,inifiles,Vcl.Imaging.pngimage;

{$R *.dfm}

const
  cCharacterSize = 8;
  cVersionString = '2018.10.31';

procedure TSDIAppForm.SaveImageAsBMP(FileName: String; Image: TImage);
begin
  Image.Picture.Bitmap.SaveToFile(ChangeFileExt(FileName,'.bmp'));
end;

procedure TSDIAppForm.SaveImageAsPNG(FileName: String; Image: TImage);
var
  Png : TPngImage;
begin
  Png := TPngImage.Create;
  try
    // copy bmp to png and save it
    Png.Assign(Image.Picture.Bitmap);
    Png.SaveToFile(ChangeFileExt(FileName,'.png'));
  finally
    Png.Free;
  end;
end;

procedure TSDIAppForm.SaveImageToFile(FileName: String; Image: TImage);
begin
  SaveImageAsPNG(FileName,Image);
end;

procedure TSDIAppForm.Welcome1Click(Sender: TObject);
begin
  DialogWelcome.ShowModal;
end;

procedure TSDIAppForm.FileSave1Execute(Sender: TObject);
begin
  SaveDialog.Filter := 'PNG Files (*.png)|*.png';
  SaveDialog.Title  := 'Save PETSCII-fied Image as';

  if not SaveDialog.Execute then Exit;

  SaveImageToFile(SaveDialog.FileName,PETSCIIFIED_Image);
end;

procedure TSDIAppForm.SetupPalettes;
begin
  // c64 palette
  C64Palette := NewPalette([
    NewRGB(0, 0, 0),
    NewRGB(255, 255, 255),
    NewRGB(136, 0, 0),
    NewRGB(170, 255, 238),
    NewRGB(204, 68, 204),
    NewRGB(0, 204, 85),
    NewRGB(0, 0, 170),
    NewRGB(238, 238, 119),
    NewRGB(221, 136, 85),
    NewRGB(102, 68, 0),
    NewRGB(255, 119, 119),
    NewRGB(51, 51, 51),
    NewRGB(119, 119, 119),
    NewRGB(170, 255, 102),
    NewRGB(0, 136, 255),
    NewRGB(187, 187, 187)
  ]);

  // black and white
  TwoTonePalette := NewPalette([
    NewRGB(0,0,0),
    NewRGB(255,255,255)
  ]);

  // 4 shades of gray
  Gray4ShadesPalette := NewPalette([
    NewRGB(255,255,255),
    NewRGB(192,192,192),
    NewRGB(128,128,128),
    NewRGB(0,0,0)
  ]);

  // 8 shades of gray
  Gray8ShadesPalette := NewPalette([
    NewRGB(0,0,0),
    NewRGB(36,36,36),
    NewRGB(72,72,72),
    NewRGB(108,108,108),
    NewRGB(144,144,144),
    NewRGB(180,180,180),
    NewRGB(216,216,216),
    NewRGB(255,255,255)
  ]);

  // 4 color: CMYK
  CMYKPalette := NewPalette([
    NewRGB(255,255,255),
    NewRGB(0,255,255),
    NewRGB(255,255,0),
    NewRGB(255,0,255),
    NewRGB(0,0,0)
  ]);

  // The original 16 color Windows palette
  WindowsPalette := NewPalette([
    NewRGB(255,255,255),
    NewRGB(192,192,192),
    NewRGB(128,128,128),
    NewRGB(0,0,0),
    NewRGB(255,0,0),
    NewRGB(128,0,0),
    NewRGB(255,255,0),
    NewRGB(128,128,0),
    NewRGB(50,205,50),
    NewRGB(0,255,0),
    NewRGB(0,255,255),
    NewRGB(0,128,128),
    NewRGB(0,0,255),
    NewRGB(0,0,128),
    NewRGB(255,0,255),
    NewRGB(128,0,128)
  ]);

  // Custom 16 color palette
  Custom16Palette := NewPalette([
    NewRGB(255,255,255),
    NewRGB(211,211,211),
    NewRGB(77,77,77),
    NewRGB(0,0,0),
    NewRGB(255,0,0),
    NewRGB(128,0,0),
    NewRGB(255,255,0),
    NewRGB(210,105,30),
    NewRGB(165,42,42),
    NewRGB(50,205,50),
    NewRGB(0,100,0),
    NewRGB(135,206,250),
    NewRGB(0,0,255),
    NewRGB(79,79,249),
    NewRGB(255,192,255),
    NewRGB(128,0,128)
  ]);

  // default
  DitheringPalette   := C64Palette;
  ReplacementPalette := Nil;
end;

procedure TSDIAppForm.ShowWelcomeOnStart;
//------------------------------------------------
// if necessary, show the welcome (using ini file settings)
//------------------------------------------------
var
  iniFile     : TIniFile;
  ShowWelcome : Boolean;
begin
  iniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  try
    if (not iniFile.SectionExists('OnLoad')) then
    // no inifile so load automatically
      DialogWelcome.ShowModal
    else begin
      ShowWelcome := iniFile.ReadBool('OnLoad','ShowWelcome',True);
      // if ShowWelcome is true in inifile then show welcome
      if ShowWelcome then
        DialogWelcome.ShowModal;
    end;
  finally
    iniFile.Free;
  end;
end;

procedure TSDIAppForm.Threshold_CheckBoxClick(Sender: TObject);
begin
  Threshold_SpinEdit.Enabled := Threshold_CheckBox.Checked;
end;

procedure TSDIAppForm.UseDithering_CheckBoxClick(Sender: TObject);
begin
  DitheringPalette_ComboBox.Enabled := UseDithering_CheckBox.Checked;
end;

procedure TSDIAppForm.FormActivate(Sender: TObject);
begin
  ShowWelcomeOnStart;
end;

procedure TSDIAppForm.FormCreate(Sender: TObject);
var
  BasePath : String;
begin
  OriginalImageSet := False;

  // set default charset pixel color for on pixels
  CharPixelOnColor.r := 255;
  CharPixelOnColor.g := 255;
  CharPixelOnColor.b := 255;
  CharPixelOnColor.a := 255;

  // set default charset pixel color for off pixels
  CharPixelOffColor.r := 0;
  CharPixelOffColor.g := 0;
  CharPixelOffColor.b := 0;
  CharPixelOffColor.a := 255;

  // load the default character set to use
  BasePath := ExtractFilePath(Application.ExeName) + 'roms\';

  LoadCharacterSet(BasePath + 'default_charset_upper.bin');

  SetupPalettes;

  PETSCIIFiedData := Nil;

  WindowState   := wsMaximized;
  DialogWelcome := TDialogWelcome.Create(Self);
  DialogWelcome.Version.Caption := 'Version: ' + cVersionString;
end;

procedure TSDIAppForm.LoadBMPFromFile(FileName: String; Image: TImage);
//------------------------------------------------
// load original image from bmp file
//------------------------------------------------
var
  bmp : TBitmap;
begin
  bmp := TBitmap.Create;
  try
    bmp.LoadFromFile(FileName);
    // round up to nearest 8 pixel dimentions
    Image.Width  := Ceil(bmp.Width/8.0)*8;
    Image.Height := Ceil(bmp.Height/8.0)*8;

    Image.Picture.Bitmap.Width  := Image.Width;
    Image.Picture.Bitmap.Height := Image.Height;

    Image.Picture.Bitmap.Canvas.StretchDraw(Rect(0,0,Image.Picture.Bitmap.Width - 1,Image.Picture.Bitmap.Height),bmp);
    Image.Picture.Bitmap.PixelFormat := pf32Bit;

    OriginalImageSet := True;
  finally
    bmp.Free;
  end;
end;

procedure TSDIAppForm.LoadPNGFromFile(FileName: String; Image: TImage);
//------------------------------------------------
// load original image from png file
//------------------------------------------------
var
  png : TPNGImage;
begin
  png := TPNGImage.Create;
  try
    png.LoadFromFile(FileName);
    // round up to nearest 8 pixel dimentions
    Image.Width  := Ceil(png.Width/8.0)*8;
    Image.Height := Ceil(png.Height/8.0)*8;

    Image.Picture.Bitmap.Width  := Image.Width;
    Image.Picture.Bitmap.Height := Image.Height;

    Image.Picture.Bitmap.Canvas.StretchDraw(Rect(0,0,Image.Picture.Bitmap.Width - 1,Image.Picture.Bitmap.Height),png);
    Image.Picture.Bitmap.PixelFormat := pf32Bit;

    OriginalImageSet := True;
  finally
    png.Free;
  end;
end;

procedure TSDIAppForm.LoadImage_ButtonClick(Sender: TObject);
//------------------------------------------------
// load original image to PETSCII-fy
//------------------------------------------------
var
  Ext: String;
begin
  OpenDialog.Title  := 'Load Image To PETSCII-fy';
  OpenDialog.Filter := 'BMP Files (*.bmp)|*.bmp|PNG Files (*.png)|*.png';
  if not OpenDialog.Execute then Exit;

  Ext := LowerCase(ExtractFileExt(OpenDialog.FileName));

  if      Ext = '.bmp' then LoadBMPFromFile(OpenDialog.FileName,Original_Image)
  else if Ext = '.png' then LoadPNGFromFile(OpenDialog.FileName,Original_Image);
end;

procedure TSDIAppForm.FileExit1Execute(Sender: TObject);
//------------------------------------------------
// exit application
//------------------------------------------------
begin
  Close;
end;

procedure TSDIAppForm.HelpAbout1Execute(Sender: TObject);
//------------------------------------------------
// set version and show about box
//------------------------------------------------
begin
  AboutBox.Version.Caption := 'Version: ' + cVersionString;
  AboutBox.ShowModal;
end;

function  TSDIAppForm.GetCharSetPixel(CharSet: PCharSet; c,x,y: Integer): Boolean;
//------------------------------------------------
// returns false/true for tbis pixel being off/on
// in the raw character set binary data
//------------------------------------------------
var
  ofs    : Integer;
  row    : Byte;
  bitmask: Integer;
begin
  // offset to character in charset
  ofs     := c * 8;
  row     := Charset[ofs + y];
  bitmask := 1 shl (7 - x);
  // return true/false if this bit is on/off
  Result := (row and bitmask) <> 0;
end;

procedure TSDIAppForm.ConvertCharSetToImage(CharSet: PCharSet; Image: TImage);
//------------------------------------------------
//convert the raw character set data to a TImage
//------------------------------------------------
var
  // image x,y
  x,y:    Integer;
  // charset character x,y
  cx,cy:  Integer;
  // destination row in image
  DstRow: PABGRArray;
  // current pixel (on/off color)
  Pixel:  TABGR;
  // the current character in the charset
  c:      Byte;
begin
  Image.Width  := 16 * cCharacterSize; // charset is 16 * 16, 8 pixel chars
  Image.Height := 16 * cCharacterSize;
  Image.Picture.Bitmap.Width  := Image.Width;
  Image.Picture.Bitmap.Height := Image.Height;
  Image.Picture.Bitmap.PixelFormat := pf32Bit;

  for y := 0 to Image.Height - 1 do begin
    DstRow := Image.Picture.Bitmap.ScanLine[y];

    for x := 0 to Image.Width - 1 do begin
      // default to black for charset bit being off
      Pixel :=   CharPixelOffColor;

      // calculate character#, x, y
      c  := (x div 8) + (y div 8)*16;
      cx := x mod 8;
      cy := y mod 8;

      // get this charset bit and set image pixel
      if (GetCharSetPixel(CharSet,c,cx,cy)) then begin
      // charset bit is set so set to on colour
        Pixel := CharPixelOnColor;
      end;

      // draw this pixel to the image
      DstRow^[x] := Pixel;
    end;
  end;

  Image.Invalidate;

  // get 8x8 block of set pixels per character in char set
  // and store in CharacterSetPixelBlocks[] array

  for c := 0 to 255 do begin
    cx := (c and $f)         * 8;
    cy := ((c shr 4) and $f) * 8;

    CharacterSetPixelBlocks[c] := GetSetPixelsBlockAt(Image,cx,cy,CharPixelOnColor);
  end;
end;

procedure TSDIAppForm.LoadCharacterSet(FileName: String);
//------------------------------------------------
//load raw character set data from a file
//------------------------------------------------
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(FileName,fmOpenRead);
  try
    fs.Read(RawCharSet,SizeOf(RawCharSet));
    ConvertCharSetToImage(@RawCharSet,CharSet_Image);
  finally
    fs.Free;
  end;
end;

procedure TSDIAppForm.LoadCharSet_ButtonClick(Sender: TObject);
//------------------------------------------------
// load charset used for PETSCII-fying
//------------------------------------------------
const
  cProgramRootName = 'petasciifier';
var
  BasePath: String;
begin
  BasePath := ExtractFilePath(Application.ExeName) + 'roms\';

  OpenDialog.Title  := 'Load Source Character Set';
  OpenDialog.Filter := 'Raw Charset Files (*.bin)|*.bin';
  OpenDialog.FileName := BasePath;
  if (OpenDialog.Execute) then
    LoadCharacterSet(OpenDialog.FileName);
end;

procedure TSDIAppForm.CopyImage_PopupMenuItemClick(Sender: TObject);
//------------------------------------------------
//copy the PETSCII-fied image to the clipboard
//------------------------------------------------
var
  MyFormat : Word;
  AData    : THandle;
  APalette : HPALETTE;
begin
  PETSCIIFIED_Image.Picture.Bitmap.SaveToClipBoardFormat(
    MyFormat,
    AData,
    APalette);
  ClipBoard.SetAsHandle(MyFormat,AData);
end;

procedure TSDIAppForm.DitheringPalette_ComboBoxClick(Sender: TObject);
//------------------------------------------------
// select dithering palette
//------------------------------------------------
begin
  case DitheringPalette_ComboBox.ItemIndex of
    0 : DitheringPalette := C64Palette;
    1 : DitheringPalette := TwoTonePalette;
    2 : DitheringPalette := Gray4ShadesPalette;
    3 : DitheringPalette := Gray8ShadesPalette;
    4 : DitheringPalette := CMYKPalette;
    5 : DitheringPalette := WindowsPalette;
    6 : DitheringPalette := Custom16Palette;
  end;
end;

procedure TSDIAppForm.CopyBufferToImage(Buffer: TImageBuffer32; Image: TImage);
//------------------------------------------------
//copy ImageBuffer32 to a TImage
//rounding up the dimensions to the nearest character size
//------------------------------------------------
var
  x,y    : Integer;
  SrcRow : PRGBAArray;
  DstRow : PABGRArray;
  bmp    : TBitmap;
begin
  // round up to nearest 8 pixel dimentions
  Image.Width  := Ceil(Buffer.Width/8.0)*8;
  Image.Height := Ceil(Buffer.Height/8.0)*8;

  Image.Picture.Bitmap.Width       := Image.Width;
  Image.Picture.Bitmap.Height      := Image.Height;
  Image.Picture.Bitmap.PixelFormat := pf32Bit;

  bmp := TBitmap.Create;

  try
    bmp.Width  := Buffer.Width;
    bmp.Height := Buffer.Height;
    bmp.PixelFormat := pf32Bit;

    for y := 0 to Buffer.Height - 1 do begin
      SrcRow := Buffer.ScanLine[y];
      DstRow := bmp.ScanLine[y];

      for x := 0 to Buffer.Width - 1 do begin
        DstRow^[x].r := SrcRow^[x].r;
        DstRow^[x].g := SrcRow^[x].g;
        DstRow^[x].b := SrcRow^[x].b;
        DstRow^[x].a := 255;
      end;
    end;

    Image.Picture.Bitmap.Canvas.StretchDraw(Rect(0,0,Image.Picture.Bitmap.Width - 1,Image.Picture.Bitmap.Height),bmp);
    Image.Picture.Bitmap.PixelFormat := pf32Bit;
  finally
    bmp.Free;
  end;

  Image.Invalidate;
end;

procedure TSDIAppForm.LoadOriginalImage(FileName: String);
var
  Buffer: TImageBuffer32;
begin
  Buffer := TImageBuffer32.Create;
  try
    Buffer.LoadFromFile(FileName);

    CopyBufferToImage(Buffer,Original_Image);

    OriginalImageSet      := True;
    PETSCIIFy_Button.Hint := 'Click to PETSCII-fy the original image!';
  finally
    Buffer.Free;
  end;
end;

procedure TSDIAppForm.InitPETSCIIfiedImage(SrcImage,DstImage: TImage);
//------------------------------------------------
// set dst image dimensions & pixelformat from src image
//------------------------------------------------
begin
  DstImage.Width                      := SrcImage.Width;
  DstImage.Height                     := SrcImage.Height;
  DstImage.Picture.Bitmap.Width       := DstImage.Width;
  DstImage.Picture.Bitmap.Height      := DstImage.Height;
  DstImage.Picture.Bitmap.PixelFormat := pf32Bit;

  DstImage.Picture.Assign(SrcImage.Picture);
  DstImage.Picture.Bitmap.PixelFormat := pf32Bit;
end;

procedure TSDIAppForm.KickAssemblyASM_PopupMenuClick(Sender: TObject);
//-------------------------------------------------------------
//export image as Kick Assembler text ASM file containing
//header, C64 character and color data
//-------------------------------------------------------------
var
  x,y    : Integer;
  w,h    : Integer;
  Text   : String;
  Writer : TStreamWriter;
begin
  h := PETSCIIFiedDataH;
  w := PETSCIIFiedDataW;

  // format header
  Text := '';
  Text := Text + Format('.const PETSCIIFiedImageWidth  = %d',[w]) + #13#10;
  Text := Text + Format('.const PETSCIIFiedImageHeight = %d',[h]) + #13#10#13#10;

  Text := Text + 'PETSCIIFiedImageCharacters:' + #13#10;

  // format character data
  for y := 0 to h - 1 do begin
    Text := Text + '  .byte ';

    for x := 0 to w - 1 do begin
      Text := Text + '$'+IntToHex(PETSCIIFiedData[y][x].characterCode,2);
      if (x < w - 1) then
        Text := Text + ',';
    end;
    Text := Text + #13#10;
  end;

  // format color data
  Text := Text + #13#10;
  Text := Text + 'PETSCIIFiedImageColors:' + #13#10;

  for y := 0 to h - 1 do begin
    Text := Text + '  .byte ';

    for x := 0 to w - 1 do begin
      Text := Text + '$'+IntToHex(PETSCIIFiedData[y][x].colorIndex,1);
      if (x < w - 1) then
        Text := Text + ',';
    end;
    Text := Text + #13#10;
  end;

  SaveDialog.Filter   := 'ASM Files (*.asm)|*.asm';
  SaveDialog.FileName := 'Untitled.asm';
  SaveDialog.Title    := 'Export as Kick Assembler ASM File';

  if (SaveDialog.Execute) then begin
    Writer := TStreamWriter.Create(ChangeFileExt(SaveDialog.FileName,'.asm'),False,TEncoding.UTF8);
    try
      Writer.Write(Text);
    finally
      Writer.Free;
    end;
  end;
end;

procedure TSDIAppForm.KickAssemblyBinaryfiles1Click(Sender: TObject);
//-------------------------------------------------------------
//export image as Kick Assembler ASM text file containing header.
//export C64 character and color data as binary files
//-------------------------------------------------------------
var
  x,y    : Integer;
  w,h    : Integer;
  Text   : String;
  Writer : TStreamWriter;
  fs     : TFileStream;
  b      : Byte;
begin
  h := PETSCIIFiedDataH;
  w := PETSCIIFiedDataW;

  Text := '';
  Text := Text + Format('.const PETSCIIFiedImageWidth  = %d',[w]) + #13#10;
  Text := Text + Format('.const PETSCIIFiedImageHeight = %d',[h]) + #13#10#13#10;

  // save header ASM file
  SaveDialog.Filter   := 'ASM Files (*.asm)|*.asm';
  SaveDialog.FileName := 'Untitled.asm';
  SaveDialog.Title    := 'Export as Kick Assembler ASM File';

  if not(SaveDialog.Execute) then Exit;

  Writer := TStreamWriter.Create(ChangeFileExt(SaveDialog.FileName,'.asm'),False,TEncoding.UTF8);
  try
    Writer.Write(Text);
  finally
    Writer.Free;
  end;

  // create c64 character data binary file
  SaveDialog.Filter   := 'Binary Files (*.bin)|*.bin';
  SaveDialog.FileName := 'character codes.bin';
  SaveDialog.Title    := 'Export as character data Binary File';

  if not(SaveDialog.Execute) then Exit;

  fs := TFileStream.Create(ChangeFileExt(SaveDialog.FileName,'.bin'),fmCreate);
  try
    for y := 0 to h - 1 do begin
      for x := 0 to w - 1 do begin
        b := Byte(PETSCIIFiedData[y][x].characterCode);
        fs.Write(b,SizeOf(b));
      end;
    end;
  finally
    fs.Free;
  end;

  // create c64 color data binary file
  SaveDialog.Filter   := 'Binary Files (*.bin)|*.bin';
  SaveDialog.FileName := 'color codes.bin';
  SaveDialog.Title    := 'Export as color data Binary File';

  if not(SaveDialog.Execute) then Exit;

  fs := TFileStream.Create(ChangeFileExt(SaveDialog.FileName,'.bin'),fmCreate);
  try
    for y := 0 to h - 1 do begin
      for x := 0 to w - 1 do begin
        b := Byte(PETSCIIFiedData[y][x].colorIndex);
        fs.Write(b,SizeOf(b));
      end;
    end;
  finally
    fs.Free;
  end;
end;

procedure TSDIAppForm.AddColorToList(AColor: TABGR; AList: TObjectList);
//------------------------------------------------
// add this color to color list if not existing.
// if existing, then increase color frequency
//------------------------------------------------
var
  Color: TColorObject;
  c:     Integer;
  Found: Boolean;
begin
  if (AList.Count = 0) then begin
  // add color to blank list
    Color := TColorObject.Create;

    Color.r         := AColor.r;
    Color.g         := AColor.g;
    Color.b         := AColor.b;
    Color.a         := 255;
    Color.frequency := 1;

    AList.Add(Color);
  end else begin
  // search for matching color and add if not there.
  // increase frequency of color if there
    Found := False;

    for c := 0 to AList.Count - 1 do begin
      Color := TColorObject(AList.Items[c]);

      if (Color.r = AColor.r) and (Color.g = AColor.g) and (Color.b = AColor.b) then begin
        Inc(Color.frequency);
        Found := True;
      end;
      if (Found) then Break;
    end;

    if (not Found) then begin
    // add color to list
      Color := TColorObject.Create;

      Color.r         := AColor.r;
      Color.g         := AColor.g;
      Color.b         := AColor.b;
      Color.a         := 255;
      Color.frequency := 1;

      AList.Add(Color);
    end;
  end;
end;

function  TSDIAppForm.GetMostUsedColourInBlockAt(SrcImage: TImage; bx,by: Integer): TABGR;
//------------------------------------------------
// get most used color in this 8x8 pixel block
//------------------------------------------------
var
  c:          Integer;
  freq:       Integer;
  x,y:        Integer;
  px,py:      Integer;
  pixel:      TABGR;
  SrcRow:     PABGRArray;
  ColorList:  TObjectList;
  Color:      TColorObject;
begin
  Result.r := 0;
  Result.g := 0;
  Result.b := 0;
  Result.a := 0;

  ColorList := TObjectList.Create;

  try
    for y := 0 to 7 do begin
      for x := 0 to 7 do begin
        px := x + bx;
        py := y + by;

        SrcRow := SrcImage.Picture.Bitmap.ScanLine[py];

        pixel := SrcRow^[px];

        AddColorToList(pixel,ColorList);
      end;
    end;

    // get color with highest frequency in block
    freq := 0;
    for c := 0 to ColorList.Count - 1 do begin
      color := TColorObject(ColorList.Items[c]);

      if (color.frequency > freq) then begin
        freq     := color.frequency;
        Result.r := color.r;
        Result.g := color.g;
        Result.b := color.b;
        Result.a := color.a;
      end;
    end;

  finally
    ColorList.Free;
  end;
end;

function  TSDIAppForm.GetSetPixelsBlockWithThresholdAt(SrcImage: TImage; bx,by: Integer; Threshold: Single): TSetPixelBlock;
//---------------------------------------------------
//  get all on/off pixels in 8x8 block >= the threshold
//---------------------------------------------------
var
  x,y:       Integer;
  px,py:     Integer;
  pixel:     TABGR;
  PixelY:    Single;
  SrcRow:    PABGRArray;
begin
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      // default to pixel off
      Result[y,x] := False;

      px := x + bx;
      py := y + by;

      SrcRow := SrcImage.Picture.Bitmap.ScanLine[py];

      pixel := SrcRow^[px];

      PixelY := 0.3*(SrcRow^[px].r/255.0)+0.6*(SrcRow^[px].g/255.0)+0.11*(SrcRow^[px].b/255.0);

      if (PixelY >= Threshold) then begin
         // this pixel color matchs so set pixel as true
        Result[y,x] := True;
      end;
    end;
  end;
end;

function  TSDIAppForm.GetSetPixelsBlockAt(SrcImage: TImage; bx,by: Integer; ColorToMatch: TABGR): TSetPixelBlock;
//---------------------------------------------------
//  get all on/off pixels in 8x8 block matching the color
//---------------------------------------------------
var
  x,y:       Integer;
  px,py:     Integer;
  pixel:     TABGR;
  SrcRow:    PABGRArray;
begin
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      // default to pixel off
      Result[y,x] := False;

      px := x + bx;
      py := y + by;

      SrcRow := SrcImage.Picture.Bitmap.ScanLine[py];

      pixel := SrcRow^[px];

      if (pixel.r = ColorToMatch.r) and
         (pixel.g = ColorToMatch.g) and
         (pixel.b = ColorToMatch.b) then begin
         // this pixel color matchs so set pixel as true
        Result[y,x] := True;
      end;
    end;
  end;
end;

function  TSDIAppForm.GetPixelsBlockMatchPercentage(ImagePixelsBlock,CharPixelsBlock: TSetPixelBlock): Integer;
//---------------------------------------------------
//  returns percentage match between blocks of 8x8 on/off pixels (true/false)s
//---------------------------------------------------
var
  x,y: Integer;

begin
  Result := 0;

  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      if (CharPixelsBlock[y,x] = ImagePixelsBlock[y,x]) then
        Inc(Result);
    end;
  end;

  // turn the count to a percentage
  Result := 100 * Result div 64;
end;

function  TSDIAppForm.GetAverageColorOfBlockAt(SrcImage: TImage; bx,by: Integer): TABGR;
//---------------------------------------------------
//  get all on/off pixels in 8x8 block matching the color
//---------------------------------------------------
var
  x,y:       Integer;
  px,py:     Integer;
  pixel:     TABGR;
  SrcRow:    PABGRArray;
  // average pixel channel colours
  r,g,b:     Integer;
begin
  r := 0;
  g := 0;
  b := 0;

  Result.a := 255;

  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      px := x + bx;
      py := y + by;

      SrcRow := SrcImage.Picture.Bitmap.ScanLine[py];

      pixel := SrcRow^[px];

      // add squares of colors
      r := r + pixel.r * pixel.r;
      g := g + pixel.g * pixel.g;
      b := b + pixel.b * pixel.b;
    end;
  end;

  // average the color result
  Result.r := Round(Sqrt(r/64));
  Result.g := Round(Sqrt(g/64));
  Result.b := Round(Sqrt(b/64));
end;

procedure TSDIAppForm.PasteImage_PopupMenuClick(Sender: TObject);
//------------------------------------------------
// paste image from clipboard into original image
//------------------------------------------------
var
  bmp : TBitmap;
  row : PABGRArray;
  x,y : Integer;
begin
  //is there a bitmap on the Windows clipboard?
  if Clipboard.HasFormat(CF_BITMAP) then
  // yes, so paste it
  begin
    bmp := TBitmap.Create;
    try
      bmp.Assign(Clipboard);
      bmp.PixelFormat := pf32Bit;

      Clipboard.Clear;

      Original_Image.Width  := Ceil(bmp.Width/8.0)*8;
      Original_Image.Height := Ceil(bmp.Height/8.0)*8;
      Original_Image.Picture.Bitmap.Width  := Original_Image.Width;
      Original_Image.Picture.Bitmap.Height := Original_Image.Height;

      Original_Image.Picture.Bitmap.Canvas.StretchDraw(Rect(0,0,Original_Image.Width,Original_Image.Height),bmp);
      Original_Image.Picture.Bitmap.PixelFormat := pf32Bit;

      Original_Image.Invalidate;

      // set transparent pixels to full
      for y := 0 to Original_Image.Height - 1 do begin
        row := Original_Image.Picture.Bitmap.ScanLine[y];

        for x := 0 to Original_Image.Width - 1 do begin
          row^[x].a := 255;
        end;
      end;

      OriginalImageSet      := True;
      PETSCIIFy_Button.Hint := 'Click to PETSCII-fy the original image!';
    finally
      bmp.Free;
    end;
  end;
end;

procedure TSDIAppForm.ReplaceImageColors(DstImage: TImage; palette: TPalette; StatusBar: TStatusBar);
//------------------------------------------------
// replace colors in image with nearest color in palette
//------------------------------------------------
var
  x,y    : Integer;
  DstRow : PABGRArray;
  i,maxI : Integer;
  index  : Integer;
begin
  i    := 0;
  maxI := DstImage.Width * DstImage.Height;

  for y := 0 to DstImage.Height - 1 do begin
    DstRow := DstImage.Picture.Bitmap.ScanLine[y];

    for x := 0 to DstImage.Width - 1 do begin
      FindNearestColor(DstRow^[x],palette,DstRow^[x],index);

      Inc(i);
    end;
    StatusBar.SimpleText := 'Replacing colors: '+IntToStr(100*i div maxI) + '%';
  end;
end;

procedure TSDIAppForm.ReplacementPalette_ComboBoxClick(Sender: TObject);
//------------------------------------------------
// set the replacemenent color palette
// used if no dithering and not using original color selection
//------------------------------------------------
begin
  case ReplacementPalette_ComboBox.ItemIndex of
    1 : ReplacementPalette := C64Palette;
    2 : ReplacementPalette := TwoTonePalette;
    3 : ReplacementPalette := Gray4ShadesPalette;
    4 : ReplacementPalette := Gray8ShadesPalette;
    5 : ReplacementPalette := CMYKPalette;
    6 : ReplacementPalette := WindowsPalette;
    7 : ReplacementPalette := Custom16Palette;
  else
    ReplacementPalette := Nil;
  end;
end;

procedure TSDIAppForm.PETSCIIFIED_ImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
//------------------------------------------------
// show PETSCII block data under mouse if applicable
//------------------------------------------------
var
  bx,by : Integer;
  Block : TPETSCIIBlock;
begin
  if (PETSCIIFiedData = Nil) then Exit;
  
  bx := Floor(X / 8.0);
  by := Floor(Y / 8.0);

  if (bx < 0) then Exit;
  if (by < 0) then Exit;
  if (bx >= PETSCIIFiedDataW) then Exit;
  if (by >= PETSCIIFiedDataH) then Exit;

  Block := PETSCIIFiedData[by][bx];

  StatusBar.SimpleText := Format('x,y: %d,%d|Code = $%x,Color = %d',[bx,by,Block.characterCode,Block.colorIndex]);
end;

function  TSDIAppForm.PETSCIIFyBlockAt(SrcImage,DstImage: TImage; bx,by: Integer; ColorToMatch,AverageColor: TABGR): TPETSCIIBlock;
//---------------------------------------------------
//  find best matching PETSCII block for this 8x8 block of pixels
//---------------------------------------------------
var
  x,y               : Integer;
  c                 : Integer;
  px,py             : Integer;
  DstRow            : PABGRArray;
  CurrentPercentage : Integer;
  CurrentCharIndex  : Integer;
  Percentage        : Integer;
  ImagePixelsBlock  : TSetPixelBlock;
  C64Color          : TABGR;
  PixelColor        : TABGR;
begin
  CurrentPercentage := 0;
  CurrentCharIndex  := 0;

  if (Threshold_CheckBox.Checked) then
    ImagePixelsBlock := GetSetPixelsBlockWithThresholdAt(SrcImage,bx,by,Threshold_SpinEdit.Value/100.0)
  else
    ImagePixelsBlock := GetSetPixelsBlockAt(SrcImage,bx,by,ColorToMatch);

  // check each charset character and find best match
  for c := 0 to 255 do begin
    Percentage := GetPixelsBlockMatchPercentage(ImagePixelsBlock,CharacterSetPixelBlocks[c]);

    if (Percentage > CurrentPercentage) then begin
      CurrentPercentage := Percentage;
      CurrentCharIndex  := c;
    end;
  end;

  // store the c64 palette character index for exporting
  Result.characterCode := CurrentCharIndex;

  // store the c64 palette color index for exporting
  if (Colors_RadioGroup.ItemIndex = 0) then begin
    FindNearestColor(AverageColor,C64Palette,C64Color,Result.colorIndex);
    PixelColor := AverageColor;
  end
  else begin
    FindNearestColor(ColorToMatch,C64Palette,C64Color,Result.colorIndex);
    PixelColor := ColorToMatch;
  end;

  // copy into image the best matching character pattern
  // along with the color
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      px := x + bx;
      py := y + by;

      DstRow := DstImage.Picture.Bitmap.ScanLine[py];

      // default to pixel off
      DstRow^[px] := CharPixelOffColor;

      if (CharacterSetPixelBlocks[CurrentCharIndex][y,x] = True) then
        // is lit pixel so use original color or average color
        DstRow^[px] := PixelColor;
    end;
  end;
end;

procedure TSDIAppForm.PETSCIIFy_ButtonClick(Sender: TObject);
//---------------------------------------------------
//  find best matching PETSCII block for each 8x8 block of pixels
//  in the image
//---------------------------------------------------
var
  sx,sy     : Integer;
  bx,by     : Integer;
  colour    : TABGR;
  AveColor  : TABGR;
  i,c       : Integer;
  w,h       : Integer;
  block     : TPETSCIIBlock;
  OldCursor : TCursor;
begin
  if not OriginalImageSet then Exit;

  OldCursor     := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    InitPETSCIIfiedImage(Original_Image,PETSCIIFIED_Image);

    UseOriginalImageColors := True;

    // if dithering, use dithering palette to dither with
    if (UseDithering_CheckBox.Checked) then begin
      floydSteinberg(PETSCIIFIED_Image,DitheringPalette,StatusBar);
      UseOriginalImageColors := False;
    end
    else
    if (ReplacementPalette <> Nil) then begin
    // if applicable, replace colors in image
      ReplaceImageColors(PETSCIIFIED_Image,ReplacementPalette,StatusBar);
      UseOriginalImageColors := False;
    end;

    //number of 8x8 blocks in image
    w := PETSCIIFIED_Image.Width  div 8;
    h := PETSCIIFIED_Image.Height div 8;

    // setup data holding PETSCII info about image
    SetLength(PETSCIIFiedData,h,w);
    PETSCIIFiedDataW := w;
    PETSCIIFiedDataH := h;

    c := w*h;
    i := 0;

    sx := 0;
    sy := 0;
    bx := 0;
    by := 0;
    repeat
      repeat
        if (UseOriginalImageColors) then begin
        // use original image colors
          colour   := GetMostUsedColourInBlockAt(Original_Image,sx,sy);
          AveColor := GetAverageColorOfBlockAt(Original_Image,sx,sy);
          block    := PETSCIIFyBlockAt(Original_Image,PETSCIIFIED_Image,sx,sy,colour,AveColor);
        end else begin
        // use colors in PETSCII-fied image
          colour   := GetMostUsedColourInBlockAt(PETSCIIFIED_Image,sx,sy);
          AveColor := GetAverageColorOfBlockAt(PETSCIIFIED_Image,sx,sy);
          block    := PETSCIIFyBlockAt(PETSCIIFIED_Image,PETSCIIFIED_Image,sx,sy,colour,AveColor);
        end;

        PETSCIIFiedData[by][bx] := block;

        inc(sx,8);
        Inc(bx);
        Inc(i);
      until sx >= Original_Image.Width;
      StatusBar.SimpleText := 'PETSCII-Fying: '+IntToStr(100*i div c) + '%';
      sx := 0;
      bx := 0;
      inc(sy,8);
      Inc(by);
    until sy >= Original_Image.Height;
    PETSCIIFIED_Image.Invalidate;

    Width_Edit.Text  := Format('%d blocks',[PETSCIIFiedDataW]);
    Height_Edit.Text := Format('%d blocks',[PETSCIIFiedDataH]);
  finally
    // reset the cursor
    Screen.Cursor := OldCursor;
  end;
end;

end.
