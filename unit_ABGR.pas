unit unit_ABGR;

interface

//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    You can find PETSCIIFier here:
//    https://bitbucket.org/paul_nicholls/petasciifier/src/master/
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

type
  PABGR = ^TABGR;
  TABGR = packed record
    case Integer of
      0 : (b,g,r,a: Byte);
      1 : (Value: LongWord);
  end;

  PABGRArray = ^TABGRArray;
  TABGRArray = array[0..(MaxInt div SizeOf(TABGR)) - 1] of TABGR;

function NewRGB(r,g,b: Integer): TABGR;
function NewRGBA(r,g,b,a: Integer): TABGR;
function NewBGRA(b,g,r,a: Integer): TABGR;

implementation

function NewRGB(r,g,b: Integer): TABGR;
begin
  Result.r := r;
  Result.g := g;
  Result.b := b;
  Result.a := 255;
end;

function NewRGBA(r,g,b,a: Integer): TABGR;
begin
  Result.r := r;
  Result.g := g;
  Result.b := b;
  Result.a := a;
end;

function NewBGRA(b,g,r,a: Integer): TABGR;
begin
  Result.r := r;
  Result.g := g;
  Result.b := b;
  Result.a := a;
end;

end.
