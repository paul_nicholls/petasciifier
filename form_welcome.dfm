object DialogWelcome: TDialogWelcome
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Welcome to PETSCII-Fier!'
  ClientHeight = 437
  ClientWidth = 723
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 85
    Width = 723
    Height = 272
    Align = alClient
    Lines.Strings = (
      
        'Welcome to PETSCII-fier, a program created by Paul Nicholls (twi' +
        'tter.com/syntaxerrorsoft) that allows one to convert an image (b' +
        'mp, png, or'
      
        'tga) to the best matching characters out of a raw C64 character ' +
        'set rom that can be loaded from a file.'
      ''
      
        'The program defaults to using the C64 PETSCII upper case charact' +
        'er set, but it also included the lower case set, '
      'and a custom 8x8 dos character set too.'
      ''
      
        'The original images to be PETSCIIfied can be loaded from file, o' +
        'r pasted from the clipboard (via menus, or right-'
      
        'click on the image).  Some work better than others (transparency' +
        ' can be an issue sometimes), but just fiddle with the options on' +
        ' the application.'
      ''
      
        'Dithering will always require a palette (defaults to the C64 pal' +
        'ette).'
      ''
      
        'No dithering + a palette will just change the image to the close' +
        'st colors in the palette.'
      ''
      
        'You can use either a grayscale threshold value or the color when' +
        ' choosing the lit pixels in the image too.'
      ''
      'After turing the image into characters, it can be:'
      '    Saved as a png file (via menu & right-click).'
      '    Copied to clipboard (menu/right-click).'
      
        '    Exported as a Kick Assembler ASM file which includes the wid' +
        'th, height, and C64 character/color data.'
      
        '    Exported as separate ASM header file (includes width, height' +
        ' of the image in 8x8 blocks), and separate C64 character/color d' +
        'ata binary files.')
    ReadOnly = True
    TabOrder = 0
    ExplicitLeft = 4
  end
  object Panel1: TPanel
    Left = 0
    Top = 357
    Width = 723
    Height = 80
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 4
    object OKBtn: TButton
      Left = 304
      Top = 48
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object ShowWelcome_CheckBox: TCheckBox
      Left = 256
      Top = 12
      Width = 177
      Height = 17
      Caption = 'Show welcome on program start'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 723
    Height = 85
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = 4
    object ProgramIcon: TImage
      Left = 8
      Top = 8
      Width = 65
      Height = 57
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000410000
        00390802000000E6E4D1BE000000017352474200AECE1CE90000000467414D41
        0000B18F0BFC6105000000097048597300000EC300000EC301C76FA864000000
        1874455874536F667477617265007061696E742E6E657420342E312E31632A9C
        4B000001C64944415478DAEDD6314BC3401407F08B8406A922B88815051141C1
        423F81A342151C14442707973AEAD282A3904EAE2E0E4E765170D07E01970E1D
        0407052D4E2A75A814155B6B7A5E39388E360DCD9983D7F2FE4338D2247DBFBE
        D72406A59474790C3480081A60040D3082061841038CA00146D0002368801134
        C0488F1A92C9A4C285D2E9B4AFE303FC1677432A95F27569DBB6150C419DA268
        F8BC3AFFB83C638BC1E5B581A5557D86FBCD38DBCE9C668337BC2636C47AF428
        A3C9C000BC7ABE5034D0AF675A2DF50D47B51A8AD96DB61D891F6B31D49E2E9C
        62DE8A268CF0987C4CE7B324EA5030904066A9923F20D577231CB1E67688D9DF
        EE47F536F045934414C4013CAE0C5776A7063648D59B43BE938D536876CBDB20
        CAF58890F082E40E787443DD507BC8386F79B1DF9C5834C717D8A25E2EFCBE5C
        87A6D7456794FBE06A903B23AB540C95DC3E71BEE58F18C3293FD272A171E6D0
        5463C03A3078FC1F9ACA950DADCDF16DA8976E7FEE4E8867CCC91533324FFEF1
        8CD3DB87A6416A172BB6CB6E593A0C01F4A175905CD3B865C5F680F6C1574110
        DF977C5D9A07D67B6BD7050D3082061841038CA00146D0002368801134C0081A
        60040D30820618E905C31F072003AD526060220000000049454E44AE426082}
      IsControl = True
    end
    object Label1: TLabel
      Left = 92
      Top = 8
      Width = 61
      Height = 13
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'PETSCII-Fier'
      IsControl = True
    end
    object Version: TLabel
      Left = 92
      Top = 25
      Width = 98
      Height = 13
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Version: 2018.10.17'
      IsControl = True
    end
    object Copyright: TLabel
      Left = 92
      Top = 47
      Width = 173
      Height = 13
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Designed and coded by Paul Nicholls'
      IsControl = True
    end
  end
end
