unit unit_bmp_loader;
{$ifdef fpc}
{$mode Delphi}
{$endif}
{$H+}

interface

//------------------------------------------------------------------------
//    This file is part of PETSCIIFier.
//
//    PETSCIIFier is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    PETSCIIFier is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with PETSCIIFier.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

uses
  unit_ImageBuffer32;

function  LoadBMPFromFile(FileName: String; Buffer: TImageBuffer32): Boolean;

implementation

uses
  Classes,
  SysUtils;

type
  //File information header
  //provides general information about the file
  TBITMAPFILEHEADER = packed record
    bfType      : Word;
    bfSize      : LongWord;
    bfReserved1 : Word;
    bfReserved2 : Word;
    bfOffBits   : LongWord;
  end;

  //Bitmap information header
  //provides information specific to the image data
  TBITMAPINFOHEADER = packed record
    biSize          : LongWord;
    biWidth         : LongInt;
    biHeight        : LongInt;
    biPlanes        : Word;
    biBitCount      : Word;
    biCompression   : LongWord;
    biSizeImage     : LongWord;
    biXPelsPerMeter : LongInt;
    biYPelsPerMeter : LongInt;
    biClrUsed       : LongWord;
    biClrImportant  : LongWord;
  end;

  //Colour palette
  TRGBQUAD = packed record
    rgbBlue     : Byte;
    rgbGreen    : Byte;
    rgbRed      : Byte;
    rgbReserved : Byte;
  end;

  PBitmapPixel = ^TBitmapPixel;
  TBitmapPixel = packed record
    b,g,r: Byte;
  end;

function LoadBMPFromFile(Filename: String;  Buffer: TImageBuffer32): Boolean;
var
  FileHeader    : TBITMAPFILEHEADER;
  InfoHeader    : TBITMAPINFOHEADER;
  Palette       : array of TRGBQUAD;
  BitmapLength  : LongWord;
  PaletteLength : LongWord;
  ReadBytes     : LongWord;
  Width,Height  : Integer;
  BitmapPixels  : PBitmapPixel;
  BitmapAddr    : PBitmapPixel;
  BufferRow     : PRGBAArray;
  x,y           : LongWord;
  BMPFile       : TFileStream;
begin
  Result := False;

  if not FileExists(FileName) then Exit;

  BMPFile := TFileStream.Create(FileName,fmOpenRead or fmShareDenyWrite);
  try
    // Get header information
    BMPFile.Read(FileHeader, SizeOf(FileHeader));
    BMPFile.Read(InfoHeader, SizeOf(InfoHeader));

    if InfoHeader.biBitCount <> 24 then
    // only supports 24 bit bitmaps!
      Exit;

    // Get palette
    PaletteLength := InfoHeader.biClrUsed;

    if PaletteLength > 0 then
    begin
      SetLength(Palette, PaletteLength);
      ReadBytes := BMPFile.Read(Palette,PaletteLength);
      if (ReadBytes <> PaletteLength) then
      begin
    //  MessageBox(0, PChar('Error reading palette'), PChar('BMP Loader'), MB_OK);
        Exit;
      end;
    end;

    Width  := InfoHeader.biWidth;
    Height := InfoHeader.biHeight;

    Buffer.SetSize(Width,Height);

    BitmapLength := InfoHeader.biSizeImage;

    if BitmapLength = 0 then
      BitmapLength := Width * Height * (InfoHeader.biBitCount Div 8);

    // Get the actual pixel data
    GetMem(BitmapPixels, BitmapLength);
    try
      ReadBytes := BMPFile.Read(BitmapPixels^, BitmapLength);
      if (ReadBytes <> BitmapLength) then
      begin
    //    MessageBox(0, PChar('Error reading bitmap data'), PChar('BMP Unit'), MB_OK);
        Exit;
      end;

      BitmapAddr := BitmapPixels;
      // copy bitmap pixels to buffer pixels
      for y := 0 to Height - 1 do
      begin
        BufferRow := Buffer.ScanLine[(Height - 1) - y];
        for x := 0 to Width - 1 do
        begin
          BufferRow^[x].r := BitmapAddr^.r;
          BufferRow^[x].g := BitmapAddr^.g;
          BufferRow^[x].b := BitmapAddr^.b;
          BufferRow^[x].a := 255;
          Inc(BitmapAddr);
        end;
      end;
    finally
      FreeMem(BitmapPixels)
    end;
  finally
    BMPFile.Free;
  end;

  Result := True;
end;

end.
